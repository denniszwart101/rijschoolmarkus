<?php

class MySettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );

    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Rijschool markus', 
            'Rijschool markus', 
            'manage_options', 
            'my-setting-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'wprm_settings' );
        ?>
        <script src="https://cdn.ckeditor.com/4.10.1/standard/ckeditor.js"></script>
        <div class="wrap">
            <h1>Rijschool markus instellingen</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'wprm_settings_group' );
                do_settings_sections( 'my-setting-admin' );
                submit_button();
            ?>
            </form>
        </div>
        <script>
            //i had to use ckeditor since wp_editor does not support html breaks.
			CKEDITOR.replace( 'wprm_settings[terms]');
            CKEDITOR.editorConfig = function( config ) {
                config.allowedContent = true
            };
		</script>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'wprm_settings_group', // Option group
            'wprm_settings', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );


        //email group
        add_settings_section(
            'wprm_email_section', // ID
            'Formulier instellingen', // Title
            array( $this), // Callback
            'my-setting-admin' // Page
        );    

        add_settings_field(
            'email', // ID
            'to: email', // Title 
            array( $this, 'email_callback' ), // Callback
            'my-setting-admin', // Page
            'wprm_email_section' // Section           
        );      

        add_settings_field(
            'success-page', // ID
            'Success page', // Title 
            array( $this, 'succes_page_callback' ), // Callback
            'my-setting-admin', // Page
            'wprm_email_section' // Section           
        );    

        add_settings_field(
            'error-page', // ID
            'Error page', // Title 
            array( $this, 'error_page_callback' ), // Callback
            'my-setting-admin', // Page
            'wprm_email_section' // Section           
        );  

        add_settings_section(
            'wprm_pdf_section', // ID
            'pdf instellingen', // Title
            array( $this, 'terms_section_text' ), // Callback
            'my-setting-admin' // Page
        );   

        add_settings_field(
            'terms', 
            'AVG Voorwaarden', 
            array( $this, 'terms_callback' ), 
            'my-setting-admin', 
            'wprm_pdf_section' //Section
        );      
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['email'] ) )
            $new_input['email'] = sanitize_text_field( $input['email'] );

        if( isset( $input['terms'] ) )
            $new_input['terms'] =  $input['terms'];

        if( isset( $input['success_page'] ) )
            $new_input['success_page'] =  $input['success_page'];

        if( isset( $input['error_page'] ) )
            $new_input['error_page'] =  $input['error_page'];    
        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function terms_section_text()
    {
        print 'Enter your settings below:';
    }

    /** 
     * Get the settings option array and print one of its values.
     */
    public function terms_callback() {

            $content = html_entity_decode($this->options['terms']);

            printf(
                '<textarea name="wprm_settings[terms]">%s</textarea>',
                isset( $this->options['terms'] ) ? esc_attr( $this->options['terms']) : '');
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function email_callback() {
        printf(
            '<input type="email" id="email" name="wprm_settings[email]" value="%s" />',
            isset( $this->options['email'] ) ? esc_attr( $this->options['email']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function succes_page_callback() {
      ?>

        <select name="wprm_settings[success_page]"> 
            <option selected="selected" disabled="disabled" value=""><?php echo esc_attr( __( 'Select page' ) ); ?></option> 
            <?php
                $selected_page = $this->options['success_page'];
                $pages = get_pages(); 
                foreach ( $pages as $page ) {
                    $option = '<option value="' . $page->ID . '" ';
                    $option .= ( $page->ID == $selected_page ) ? 'selected="selected"' : '';
                    $option .= '>';
                    $option .= $page->post_title;
                    $option .= '</option>';
                    echo $option;
                }
            ?>
        </select>

      <?php
    }


    /** 
     * Get the settings option array and print one of its values
     */
    public function error_page_callback() {
        ?>
  
          <select name="wprm_settings[error_page]"> 
              <option selected="selected" disabled="disabled" value=""><?php echo esc_attr( __( 'Select page' ) ); ?></option> 
              <?php
                  $selected_page = $this->options['error_page'];
                  $pages = get_pages(); 
                  foreach ( $pages as $page ) {
                      $option = '<option value="' . $page->ID . '" ';
                      $option .= ( $page->ID == $selected_page ) ? 'selected="selected"' : '';
                      $option .= '>';
                      $option .= $page->post_title;
                      $option .= '</option>';
                      echo $option;
                  }
              ?>
          </select>
  
        <?php
      }

}

if( is_admin() )
    $my_settings_page = new MySettingsPage();