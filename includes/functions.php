<?php

function sendContractToAdmin() {
    
    $options = get_option( 'wprm_settings');

    $params = [
        'first_name' => isset($_POST['firstName']) ? $_POST['firstName'] : '',
        'last_name' => isset($_POST['lastName']) ? $_POST['lastName'] : '',
        'tel' => isset($_POST['tel']) ? $_POST['tel'] : '',
        'email' => isset($_POST['email']) ? $_POST['email'] : '',
        'street' => isset($_POST['street']) ? $_POST['street'] : '',
        'number' => isset($_POST['number']) ? $_POST['number'] : '',
        'zip_code' => isset($_POST['zipCode']) ? $_POST['zipCode'] : '',
        'city' => isset($_POST['city']) ? $_POST['city'] : '',
        'date_birth' => isset($_POST['date']) ? $_POST['date'] : '',
    ];
    
    $pdf = new generatePdf();
    
    $attachement = $pdf->savePdf($params);


   if (!is_null($attachement) &&  function_exists('wp_mail') ) {
    $to = $options['email'];
    $subject = "Nieuwe avg overeenkomst";
    $message = "<html><body><p>er is een nieuw avg overeenkomst ingevuld.</p></body></html>";
    $headers = array('Content-Type: text/html; charset=UTF-8');
    $mailSent = false;
    $mailSent = wp_mail( $to, $subject, $message, $headers, $attachement );
    if ( $mailSent ) {
        wp_redirect(get_permalink($options['success_page']));
        exit;
    } else {
        wp_redirect(get_permalink($options['error_page']));
        exit;
    }

   } else {
    wp_redirect(get_permalink($options['error_page']));
    exit;
   }
    



}


add_action( 'admin_post_nopriv_avg_form', 'sendContractToAdmin' );
add_action( 'admin_post_avg_form', 'sendContractToAdmin');