<?php

require_once(dirname(__file__) . '/dompdf/dompdf_config.inc.php');

class generatePdf {

    /*
        create/save the pdf and return the file path. 
    */
    public function savePdf($params) {
        try {

            $pluginOptions = get_option('wprm_settings');

            $style = '
            <style>
            
            .page_break {
                page-break-before: always;
            }
            </style>';
        
            $dompdf = new DOMPDF();
            $overeenkomst = $style . $pluginOptions['terms'];

            $dompdf->load_html($this->str_replace($overeenkomst, $params), 'UTF-8');

            $dompdf->render();

            $name = $params['first_name'] . '_' . $params['last_name'];

            file_put_contents(CLASSDIR . '/pdf_log/'. $name .'_overeenkomst.pdf',$dompdf->output());
            return CLASSDIR . '/pdf_log/'. $name .'_overeenkomst.pdf';
        } catch (Exception $e) {

        }
    }
    /*
        Replace the strings with filled content.
    */
    private function str_replace($terms, $params) {

        $replace  = [
            '{first_name}' => $params['first_name'],
            '{last_name}' => $params['last_name'],
            '{tel}' => $params['tel'],
            '{email}' => $params['email'],
            '{street}' => $params['street'],
            '{housenr}' => $params['number'],
            '{zip_code}' => $params['zip_code'],
            '{city}' => $params['city'],
            '{date_birth}' => $params['date_birth'],
            '{current_date}' => Date('m/d/Y'),
            '{new_page}' => '<div class="page_break"></div>'
        ];

        return $string = strtr($terms, $replace); 

    }


}