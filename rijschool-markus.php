<?php
/*
Plugin Name: Rijschool Markus
Plugin URI: https://rijschoolmarkus.nl/
Description: Nieuwe leerling registratie formulier
Version: 1.0.0
Author: Dennis Zwart
Text Domain: WPRM
Author URI: https://rijschoolmarkus.nl/
*/
defined( 'ABSPATH' ) or die( 'Error!' );



define('CLASSDIR', plugin_dir_path( __FILE__ ));
define('PLUGINDIR', plugin_dir_url(__FILE__));


require_once(plugin_dir_path( __file__ ) . '/includes/MySettingsPage.php');

require_once(plugin_dir_path( __FILE__ ) . 'PageTemplate.php');
require_once(plugin_dir_path( __file__ ) . '/includes/tcpdf/tcpdf.php');
require_once(plugin_dir_path( __file__ ) . '/includes/generatePdf.php');
require_once(plugin_dir_path( __file__ ) . '/includes/functions.php');





