<?php
/*
 * Template Name: Student Registration
 * Description: Custom Student Registration plugin
 */
?>


<?php
//include bootstrap styles
    wp_register_style('prefix_bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css');
    wp_enqueue_style('prefix_bootstrap');

//include jQuery
    wp_register_script('prefix_jQuery', '//code.jquery.com/jquery-3.2.1.slim.min.js');
    wp_enqueue_script('prefix_jQuery');

//get theme header
    get_header();
?>

    <!-- main content -->
    <section class="section white">
	    <div class="inner">
            <div class="container">
                
                <!-- display page text -->
                <?php 
                if ( have_posts() ) while ( have_posts() ) : the_post();
                
                    the_content();
                
                endwhile; ?>

                <!-- registration form start -->
                <form class="needs-validation" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" novalidate>
                    
                    <!--  Name Values -->
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">Voornaam</label>
                            <input type="text" class="form-control" name="firstName" required>
                            <div class="invalid-feedback">
                                Vul een voornaam in
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastName">Achternaam</label>
                            <input type="text" class="form-control" name="lastName" required>
                        </div>
                    </div>
                    <!-- Contact values -->
                     <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="tel">Telefoonnummer</label>
                            <input type="text" class="form-control" name="tel" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="email">E-mailadres</label>
                            <input type="email" class="form-control" name="email" required>
                        </div>
                    </div>

                    <!-- address values -->
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="street">Straat</label>
                            <input type="text" class="form-control" name="street" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="number">Huisnr.</label>
                            <input type="text" class="form-control" name="number" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="zipCode">Postcode</label>
                            <input type="text" class="form-control" name="zipCode" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="city">Plaats</label>
                            <input type="text" class="form-control" name="city" required>
                        </div>
                    </div>
                    <div class="form-row">
                        
                        <div class="col-md-6 mb-3">
                            <label for="date">Geboorte datum</label>
                            <input type="date" class="form-control" name="date" required>
                        </div>


                    </div>

                    <div class="custom-control custom-checkbox mb-6">
                        <input type="checkbox" class="custom-control-input" id="aggreed" required>
                        <label class="custom-control-label" for="aggreed"><a href="https://google.nl" target="_blanc">Ik ga akkoord met de AVG voorwaarden</a></label>
                        <div class="invalid-feedback">U moet akkoord gaan met de AVG voorwaarden om door te kunnen</div>
                    </div>
                    <input type="hidden" name="action" value="avg_form">*
                   <input type="submit" value="Versturen" class="wpcf7-form-control wpcf7-submit pull-right">
                </form>
                <!-- registration form end -->
            </div>
        </div>
    </section>
<?php

add_action( 'wp_footer', function() { ?>

	<script>
        (function() {
        'use strict';
            window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
            });
        }, false);
        })();
    </script>

<?php } );



//get theme footer
get_footer();



?>
